import 'package:flutter/material.dart';
import 'package:mywebdemo/Navbar/NavBarCustomContainer.dart';


class DeskNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75,
      color: Colors.black,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Image.asset("assets/logo.png"),
          ),
          // buildContainer(Colors.black,"NAROSUNDAR",16,FontWeight.w900),
          Spacer(),
          buildContainer(Colors.black,"HOME",14,FontWeight.w500,Colors.red),
          buildContainer(Colors.black,"ABOUT",14,FontWeight.w500,Colors.red),
          buildContainer(Colors.black,"SERVICE",14,FontWeight.w500,Colors.red),
          buildContainer(Colors.black,"GALLERY",14,FontWeight.w500,Colors.red),
          buildContainer(Colors.black,"CONTACT",14,FontWeight.w500,Colors.red),
          buildContainer(Colors.red,"FREE QUOTE",14,FontWeight.w500,Colors.black),
        ],
      ),
    );
  }
}
