import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';

Widget buildContainer(
    Color color, String text, double fontSize, FontWeight fontWeight,Color color1) {
  // return container(color,Colors.red, text, fontSize, fontWeight);
  return HoverWidget(
    onHover: (e){},
      hoverChild: container(color, color1, text, fontSize, fontWeight),
      child: container(color, Colors.white, text, fontSize,
          fontWeight)); //child: container(color, text, fontSize, fontWeight));
}

Container container(Color color, Color color1, String text, double fontSize,
    FontWeight fontWeight) {
  return Container(
    color: color,
    child: Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              text,
              style: TextStyle(
                  color: color1, fontSize: fontSize, fontWeight: fontWeight),
            ),
          ),
        ),
      ],
    ),
  );
}


