

import 'package:flutter/material.dart';

class AnimationPage extends StatefulWidget {
  AnimationPage({this.child,this.child1,this.value
  });
  Widget child;
  Widget child1;
  double value;
  @override
  _AnimationPageState createState() => _AnimationPageState();
}

class _AnimationPageState extends State<AnimationPage>with SingleTickerProviderStateMixin {
  bool value =false;
  bool showPicker = false;
  AnimationController _controller;
  Animation _animation;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return  MouseRegion(
      onHover: (e){
        _controller.forward();
      },
      onExit: (e){
        _controller.reverse();
      },
      child: Container(
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: [
            widget.child,
            SizeTransition(
              sizeFactor: _animation,
              axisAlignment: widget.value,
              axis: widget.value==1?Axis.vertical:Axis.horizontal,
              child: widget.child1,
            ),
          ],
        ),
      ),
    );
  }
}
