import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class VeryGoodWidget extends StatelessWidget {
  final Widget child;

  const VeryGoodWidget(this.child);

  @override
  Widget build(BuildContext context) {
    return VeryGoodWrapper(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: child,
      ),
    );
  }
}

class VeryGoodWrapper extends StatelessWidget {
  final Widget child;

  const VeryGoodWrapper({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ScrollController>(
      builder: (context, scrollController, child) {
        final renderObject = context.findRenderObject() as RenderBox;
        final offsetY = renderObject?.localToGlobal(Offset.zero)?.dy ?? 0;
        if (offsetY <= 0) {
          return child;
        }
        final deviceHeight = MediaQuery.of(context).size.height;
        final size = renderObject.size;
        final heightVisible = deviceHeight - offsetY;
        final howMuchSown = (heightVisible / size.height).clamp(0.0, 1.0);
        final scale = 0.8 + howMuchSown * 0.2;
        final opacity = 0.25 + howMuchSown * 0.75;
        return Transform.scale(
          scale: scale,
          alignment: Alignment.center,
          child: Opacity(
            opacity: opacity,
            child: child,
          ),
        );
      },
      child: child,
    );
  }
}