// import 'package:flutter/material.dart';
//
// void main() => runApp(MyApp());
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: Scaffold(
//         body: GridExample(),
//       ),
//     );
//   }
// }
//
// class GridExample extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return GridView.builder(
//       itemCount: 10,
//       itemBuilder: (ctx, ind) {
//         return _Item('${ind + 1},000,000');
//       },
//       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//           crossAxisCount: 3, childAspectRatio: 3),
//     );
//   }
// }
//
// class _Item extends StatelessWidget {
//   _Item(this.title);
//   String title;
//
//   final GlobalKey _globalKey = GlobalKey();
//
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       key: _globalKey,
//       onTap: () async {
//         final RenderBox referenceBox =
//         _globalKey.currentContext.findRenderObject();
//         final position = referenceBox.localToGlobal(Offset.zero);
//         final x = position.dx;
//         final y = position.dy;
//         final w = referenceBox.size.width;
//         Future.delayed(Duration(milliseconds: 100))
//             .then((_) => _particleFieldLineExplosion(x, y, w));
//       },
//       child: Center(child: Text(title)),
//     );
//   }
//
//   _particleFieldLineExplosion(x, y, w) {
//     print(x);
//     print(y);
//     print(w);
//   }
// }