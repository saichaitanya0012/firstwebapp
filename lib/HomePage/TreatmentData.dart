
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

Column beardTreatment(String text1, String text2,String text3,context) {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Row(
          children: [
            Text(
              text1,
              style: TextStyle(
                  fontSize: 3.w,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            Spacer(),
            Row(
              children: [
                Text(
                  text2+" ",
                  style: TextStyle(
                      fontSize: 3.w,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  text3,
                  style: TextStyle(
                      fontSize: 3.w,
                      color: Colors.red,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Divider(
          height: 1,
          color: Colors.black26,
        ),
      ),
    ],
  );
}
