import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mywebdemo/AnimationPage.dart';
import 'package:mywebdemo/HomePage/SlideAnimation.dart';
import 'package:mywebdemo/HomePage/Story.dart';
import 'package:mywebdemo/HomePage/TreatmentData.dart';
import 'package:mywebdemo/HomePage/footer.dart';
import 'package:mywebdemo/Navbar/MobileNavBar.dart';
import 'package:mywebdemo/animation/VeryGoodWidget.dart';

import 'Review.dart';

class MobileHomeScreen extends StatefulWidget {
  dynamic scrollController;

  MobileHomeScreen({this.scrollController});

  @override
  _MobileHomeScreenState createState() => _MobileHomeScreenState();
}

class _MobileHomeScreenState extends State<MobileHomeScreen>
    with SingleTickerProviderStateMixin {
  bool value = false;
  bool showPicker = false;
  AnimationController _controller;
  Animation _animation;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget buildMouseRegion() {
    return SizeTransition(
      sizeFactor: _animation,
      axisAlignment: 1,
      axis: Axis.vertical,
      child: Container(
        color: Colors.white,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  MouseRegion(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "HOME",
                        style: TextStyle(
                            color: Colors.yellow,
                            fontWeight: FontWeight.w300,
                            fontSize: 16),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "ABOUT",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w300,
                          fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "SERVICE",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w300,
                          fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "GALLERY",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w300,
                          fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "CONTACT",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w300,
                          fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        color: Colors.red,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 20),
                          child: Text(
                            "FREE QUOTE",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w300,
                                fontSize: 16),
                          ),
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: widget.scrollController,
      children: [
        Column(
          children: [
            Container(
              height: 75,
              width: MediaQuery.of(context).size.width,
              color: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Image.asset(
                      "assets/logo.png",
                      height: MediaQuery.of(context).size.width * 0.2,
                      width: MediaQuery.of(context).size.width * 0.2,
                    ),
                  ),
                  // Spacer(),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: InkWell(
                      onTap: () {
                        value = !value;
                        setState(() {});
                        value ? _controller.forward() : _controller.reverse();
                      },
                      child: Icon(
                        Icons.menu,
                        color: Colors.white,
                        size: MediaQuery.of(context).size.width * 0.05,
                      ),
                    ),
                  )
                ],
              ),
            ),
            //
          ],
        ),
        buildMouseRegion(),
        Container(
            color: Colors.black,
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: DelayedDisplay(
                delay: Duration(
                  milliseconds: 500,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: SvgPicture.asset("assets/scissors.svg"),
                    ),
                    Text(
                      "WE'RE THE LAST OF BREED",
                      style: TextStyle(
                          fontSize: 55,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        "Archive your dream style",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                            border: Border.all(color: Colors.red)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Text(
                            "BOOK NOW",
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.red,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )),
        VeryGoodWidget(
          AnimationPage(
            child: padding(
                "assets/services1.svg",
                "Stylish hair cut",
                "Available be the majority have suffered alteration in some form, by injected humour.",
                Colors.black,
                Colors.white),
            child1: padding(
                "assets/services1.svg",
                "Stylish hair cut",
                "Available be the majority have suffered alteration in some form, by injected humour.",
                Colors.white,
                Colors.black),
            value: 1,
          ),
        ),
        VeryGoodWidget(
          AnimationPage(
            child: padding(
                "assets/services2.svg",
                "Stylish hair cut",
                "Available be the majority have suffered alteration in some form, by injected humour.",
                Colors.black,
                Colors.white),
            child1: padding(
                "assets/services2.svg",
                "Stylish hair cut",
                "Available be the majority have suffered alteration in some form, by injected humour.",
                Colors.white,
                Colors.black),
            value: 1,
          ),
        ),
        VeryGoodWidget(
          AnimationPage(
            child: padding(
                "assets/services3.svg",
                "Stylish hair cut",
                "Available be the majority have suffered alteration in some form, by injected humour.",
                Colors.black,
                Colors.white),
            child1: padding(
                "assets/services3.svg",
                "Stylish hair cut",
                "Available be the majority have suffered alteration in some form, by injected humour.",
                Colors.white,
                Colors.black),
            value: 1,
          ),
        ),
        VeryGoodWidget(
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: buildColumn(),
          ),
        ),
        VeryGoodWidget(
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: image1(),
          ),
        ),
        // story(),
        VeryGoodWidget(
          Column(
            children: [
              Row(
                children: [
                  Expanded(child: Image.asset("assets/barber1.jpg")),
                  Expanded(child: Image.asset("assets/barber2.jpg")),
                  Expanded(child: Image.asset("assets/barber3.jpg")),
                ],
              ),
              Row(
                children: [
                  Expanded(child: Image.asset("assets/barber4.jpg")),
                  Expanded(child: Image.asset("assets/barber5.jpg")),
                  Expanded(child: Image.asset("assets/barber6.jpg")),
                ],
              )
            ],
          ),
        ),

        VeryGoodWidget(
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 50,
                  height: 10,
                  color: Colors.red,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text(
                    "OUR TOP PRICES",
                    style: TextStyle(
                        fontSize: 55,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Divider(
                    height: 2,
                    color: Colors.black,
                  ),
                ),
                beardTreatment(
                    "Special Beard Treatment", "From ", "\$40", context),
                beardTreatment(
                    "Special Beard Treatment", "From ", "\$40", context),
                beardTreatment(
                    "Color your Beard", "From", " \$40", context),
                beardTreatment("Wax your Beard", "From", " \$40", context),
                beardTreatment("Wax your Beard", "From", " \$40", context),
              ],
            ),
          ),
        ),
        VeryGoodWidget(
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 50,
                  height: 10,
                  color: Colors.red,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text(
                    "CUSTOMERS SAY ABOUT US",
                    style: TextStyle(
                        fontSize: 55,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                buildContainer(false),
                buildContainer(true),
                buildContainer(false),
              ],
            ),
          ),
        ),
        Container(
          color: Colors.black,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              contactInfo(Icons.phone),
              siteInfo(),
              contactInfo(Icons.navigation),
              Divider(
                color: Colors.white38,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: Center(
                  child: Text(
                    "Copyright ©2021 All rights reserved | This template is made with  by Colorlib",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                        color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

// mobileHomeScreen(context, s) {
//   return Stack(
//     children: [
//       ListView(
//         controller: s,
//         children: [
//           SizedBox(
//             height: 75,
//           ),
//           Container(
//               color: Colors.black,
//               child: Padding(
//                 padding: const EdgeInsets.all(18.0),
//                 child: DelayedDisplay(
//                   delay: Duration(
//                     milliseconds: 500,
//                   ),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Padding(
//                         padding: const EdgeInsets.only(bottom: 10),
//                         child: SvgPicture.asset("assets/scissors.svg"),
//                       ),
//                       Text(
//                         "WE'RE THE LAST OF BREED",
//                         style: TextStyle(
//                             fontSize: 55,
//                             color: Colors.white,
//                             fontWeight: FontWeight.bold),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.only(top: 10),
//                         child: Text(
//                           "Archive your dream style",
//                           style: TextStyle(
//                               fontSize: 20,
//                               color: Colors.white,
//                               fontWeight: FontWeight.bold),
//                         ),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(8.0),
//                         child: Container(
//                           decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(10),
//                               color: Colors.white,
//                               border: Border.all(color: Colors.red)),
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 20, vertical: 10),
//                             child: Text(
//                               "BOOK NOW",
//                               style: TextStyle(
//                                   fontSize: 20,
//                                   color: Colors.red,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               )),
//           VeryGoodWidget(
//             AnimationPage(
//               child: padding(
//                   "assets/services1.svg",
//                   "Stylish hair cut",
//                   "Available be the majority have suffered alteration in some form, by injected humour.",
//                   Colors.black,
//                   Colors.white),
//               child1: padding(
//                   "assets/services1.svg",
//                   "Stylish hair cut",
//                   "Available be the majority have suffered alteration in some form, by injected humour.",
//                   Colors.white,
//                   Colors.black),
//               value: 1,
//             ),
//           ),
//           VeryGoodWidget(
//             AnimationPage(
//               child: padding(
//                   "assets/services2.svg",
//                   "Stylish hair cut",
//                   "Available be the majority have suffered alteration in some form, by injected humour.",
//                   Colors.black,
//                   Colors.white),
//               child1: padding(
//                   "assets/services2.svg",
//                   "Stylish hair cut",
//                   "Available be the majority have suffered alteration in some form, by injected humour.",
//                   Colors.white,
//                   Colors.black),
//               value: 1,
//             ),
//           ),
//           VeryGoodWidget(
//             AnimationPage(
//               child: padding(
//                   "assets/services3.svg",
//                   "Stylish hair cut",
//                   "Available be the majority have suffered alteration in some form, by injected humour.",
//                   Colors.black,
//                   Colors.white),
//               child1: padding(
//                   "assets/services3.svg",
//                   "Stylish hair cut",
//                   "Available be the majority have suffered alteration in some form, by injected humour.",
//                   Colors.white,
//                   Colors.black),
//               value: 1,
//             ),
//           ),
//           VeryGoodWidget(
//             Padding(
//               padding: const EdgeInsets.all(40.0),
//               child: buildColumn(),
//             ),
//           ),
//           VeryGoodWidget(
//             Padding(
//               padding: const EdgeInsets.all(50.0),
//               child: image1(),
//             ),
//           ),
//           // story(),
//           VeryGoodWidget(
//             Column(
//               children: [
//                 Row(
//                   children: [
//                     Expanded(child: Image.asset("assets/barber1.jpg")),
//                     Expanded(child: Image.asset("assets/barber2.jpg")),
//                     Expanded(child: Image.asset("assets/barber3.jpg")),
//                   ],
//                 ),
//                 Row(
//                   children: [
//                     Expanded(child: Image.asset("assets/barber4.jpg")),
//                     Expanded(child: Image.asset("assets/barber5.jpg")),
//                     Expanded(child: Image.asset("assets/barber6.jpg")),
//                   ],
//                 )
//               ],
//             ),
//           ),
//
//           VeryGoodWidget(
//             Padding(
//               padding: const EdgeInsets.all(50.0),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: [
//                   Container(
//                     width: 50,
//                     height: 10,
//                     color: Colors.red,
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 20),
//                     child: Text(
//                       "OUR TOP PRICES",
//                       style: TextStyle(
//                           fontSize: 55,
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 20),
//                     child: Divider(
//                       height: 2,
//                       color: Colors.black,
//                     ),
//                   ),
//                   beardTreatment(
//                       "Special Beard Treatment", "From ", "\$40", context),
//                   beardTreatment(
//                       "Special Beard Treatment", "From ", "\$40", context),
//                   beardTreatment("Color your Beard", "From", " \$40", context),
//                   beardTreatment("Wax your Beard", "From", " \$40", context),
//                   beardTreatment("Wax your Beard", "From", " \$40", context),
//                 ],
//               ),
//             ),
//           ),
//           VeryGoodWidget(
//             Padding(
//               padding: const EdgeInsets.all(50.0),
//               child: Column(
//                 // crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Container(
//                     width: 50,
//                     height: 10,
//                     color: Colors.red,
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 20),
//                     child: Text(
//                       "CUSTOMERS SAY ABOUT US",
//                       style: TextStyle(
//                           fontSize: 55,
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                   buildContainer(false),
//                   buildContainer(true),
//                   buildContainer(false),
//                 ],
//               ),
//             ),
//           ),
//           Container(
//             color: Colors.black,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 contactInfo(Icons.phone),
//                 siteInfo(),
//                 contactInfo(Icons.navigation),
//                 Divider(
//                   color: Colors.white38,
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.symmetric(vertical: 30),
//                   child: Center(
//                     child: Text(
//                       "Copyright ©2021 All rights reserved | This template is made with  by Colorlib",
//                       style: TextStyle(
//                           fontSize: 16,
//                           fontWeight: FontWeight.w300,
//                           color: Colors.white),
//                       textAlign: TextAlign.center,
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//
//       // MobileNavBar(),
//
//       Container(
//         height: 75,
//         width: MediaQuery.of(context).size.width,
//         color: Colors.black,
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             Padding(
//               padding: const EdgeInsets.only(left: 10),
//               child: Image.asset(
//                 "assets/logo.png",
//                 height: MediaQuery.of(context).size.width * 0.2,
//                 width: MediaQuery.of(context).size.width * 0.2,
//               ),
//             ),
//             // Spacer(),
//
//             Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 20),
//               child: InkWell(
//                 onTap: () {
//                   // value=!value;
//                   // setState(() {
//                   //
//                   // });
//                   // value?_controller.forward(): _controller.reverse();
//                 },
//                 child: Icon(
//                   Icons.menu,
//                   color: Colors.white,
//                   size: MediaQuery.of(context).size.width * 0.05,
//                 ),
//               ),
//             )
//           ],
//         ),
//       ),
//     ],
//   );
// }
