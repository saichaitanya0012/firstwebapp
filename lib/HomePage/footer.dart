import 'package:flutter/material.dart';

Widget contactInfo(IconData data) {
  return Container(
    width: 300,
    // color: Colors.yellow,
    child: Column(
      // mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(
            data,
            color: Colors.red,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "CONTACT INFO",
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "913-473-7000",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w300,
                color: Colors.white),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "contact@cakeshop.com",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w300,
                color: Colors.white),
          ),
        )
      ],
    ),
  );
}

Widget siteInfo() {
  return Column(
    children: [
      Image.asset("assets/logo2_footer.png"),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "There are many variations of passages of Lorem Ipsum available be the majority.",
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w300,
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
      )
    ],
  );
}