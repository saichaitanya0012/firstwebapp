import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

Widget padding(String string,String string1,String string2,Color color,Color color1) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      decoration: BoxDecoration(color: color,

        // border: BorderRadius.all(Radius.circular(20));
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),

      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SvgPicture.asset(string),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                string1,
                style: TextStyle(fontSize: 24, color: color1),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                string2,
                style: TextStyle(fontSize: 16, color: color1),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    ),
  );
}