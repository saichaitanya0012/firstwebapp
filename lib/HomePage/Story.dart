
import 'package:flutter/material.dart';
import 'package:mywebdemo/AnimationPage.dart';
import 'package:sizer/sizer.dart';

Widget story() {
  return Padding(
      padding: const EdgeInsets.all(50.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 50,
            height: 10,
            color: Colors.red,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              "ABOUT OUR STORY",
              style: TextStyle(
                  fontSize: 55,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              "There are many variations of passages of Lorem Ipsum available be the majority have suffered alteration in some form, by injected humour or randomised words.",
              style: TextStyle(
                  fontSize: 2.w,
                  color: Colors.black,
                  fontWeight: FontWeight.w300),
            ),
          ),


          bookNow()

        ],
      ),

  );
}

Padding bookNow() {
  return Padding(
        padding: const EdgeInsets.only(top: 30),
        child: AnimationPage(child: container(Colors.red,Colors.white,),child1: container(Colors.white,Colors.red),value: -1,)
      );
}

Container container(Color color,Color color1) {
  return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: color1,
          border: Border.all(color: Colors.red)
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child: Text(
            "BOOK NOW",
            style: TextStyle(
                fontSize: 20,
                color: color,
                fontWeight: FontWeight.bold),
          ),
        ),
      );
}

Widget image1() {
  return  Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Image.asset("assets/about1.jpg"),

  );
}

Widget buildColumn() {
  return  Column(
      children: [
        buildRow(
            "Be the majority have suffered alteration in some form, by injected humour."),
        buildRow(
            "Psum available be the majority have suffered alteration in some form, by injected humour."),
        buildRow(
            "Available be the majority have suffered alteration in some form, by injected humour."),
        buildRow(
            "Humour available be the majority have suffered alteration in some form, by injected."),
      ],
  );
}

Widget buildRow(String text) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundColor: Colors.red[100],
          radius: 3.w,
          child: Icon(
            Icons.check,
            color: Colors.red,
            size: 3.w,
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              text,
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w300),
            ),
          ),
        ),
      ],
    ),
  );
}

