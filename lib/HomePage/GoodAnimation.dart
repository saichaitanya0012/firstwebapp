import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';

// class VerGood extends StatefulWidget {
//   VerGood({this.child,this.controller});
//   final Widget child;
//   final ScrollController controller;
//   @override
//   _VerGoodState createState() => _VerGoodState();
// }
//
// class _VerGoodState extends State<VerGood> {
//   @override
//   Widget build(BuildContext context) {
//     return AnimatedBuilder(
//       animation: widget.controller,
//       builder: (context, child) {
//         final renderObject = context.findRenderObject() as RenderBox;
//         final offsetY = renderObject?.localToGlobal(Offset.zero)?.dy ?? 0;
//         if (offsetY <= 0) {
//           return child;
//         }
//         final deviceHeight = MediaQuery.of(context).size.height;
//         final heightVisible = deviceHeight - offsetY;
//         final widgetHeight = renderObject.size.height;
//         final howMuchShown = (heightVisible / widgetHeight).clamp(0.0, 1.0);
//         final scale = 0.8 + howMuchShown * 0.2;
//         final opacity = 0.25 + howMuchShown * 0.75;
//         return Transform.scale(
//           scale: scale,
//           alignment: Alignment.center,
//           child: Opacity(
//             opacity: opacity,
//             child: child,
//           ),
//         );
//       },
//       child: widget.child,
//     );
//   }
// }


class demoData extends StatefulWidget {
  @override
  _demoDataState createState() => _demoDataState();
}

class _demoDataState extends State<demoData> {
  ScrollController scrollController=ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: ListView(
          cacheExtent: 300,
          // primary: false,
          shrinkWrap: true,
          // physics: true,
          // cacheExtent: 3500,
          // primary: false,
          // shrinkWrap: false,
          // controller: scrollController,
          children: [
            DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.orange,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.amber,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.black54,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.red,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.yellow,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.black26,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.blue,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors. orange,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ), DelayedDisplay(
              delay: Duration(seconds: 1),
              child: Container(
                height:200,width:200,color: Colors.green,),
            ),
          ],
        ),
      )

      // ListView(
      //   // mainAxisSize: MainAxisSize.max,
      //   children: [
      //     Container(
      //         height:200,width:200,child: GoodAnimation()),Container(
      //         height:200,width:200,child: GoodAnimation()),Container(
      //         height:200,width:200,child: GoodAnimation()),Container(
      //         height:200,width:200,child: GoodAnimation()),Container(
      //         height:200,width:200,child: GoodAnimation()),Container(
      //         height:200,width:200,child: GoodAnimation()),Container(
      //         height:200,width:200,child: GoodAnimation()),
      //   ],
      // ),
    );
  }
}


class GoodAnimation extends StatelessWidget {
  final Widget child = Container(
    height: 200,
    width: 20,
    color: Colors.green,
  );

  final ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedBuilder(
        animation: controller,
        builder: (context, child) {
          final renderObject = context.findRenderObject() as RenderBox;
          final offsetY = renderObject?.localToGlobal(Offset.zero)?.dy ?? 0;
          if (offsetY <= 0) {
            return child;
          }
          final deviceHeight = MediaQuery.of(context).size.height;
          final heightVisible = deviceHeight - offsetY;
          final widgetHeight = renderObject.size.height;
          final howMuchShown = (heightVisible / widgetHeight).clamp(0.0, 1.0);
          final scale = 0.8 + howMuchShown * 0.2;
          final opacity = 0.25 + howMuchShown * 0.75;
          return Transform.scale(
            scale: scale,
            alignment: Alignment.center,
            child: Opacity(
              opacity: opacity,
              child: child,
            ),
          );
        },
        child: Container(
            height:200,width:200,color: Colors.green,),
      ),
    );
  }
}
