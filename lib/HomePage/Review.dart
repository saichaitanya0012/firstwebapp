import 'package:flutter/material.dart';

Widget buildContainer(bool value) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(width: 1.0, color: Colors.black26),
        bottom: BorderSide(width: 1.0, color: Colors.black26),
        right: !value
            ? BorderSide(width: 1.0, color: Colors.black26)
            : BorderSide(width: 0.0, color: Colors.white),
        left: value
            ? BorderSide(width: 1.0, color: Colors.black26)
            : BorderSide(width: 0.0, color: Colors.white),
      ),
      color: Colors.white,
    ),
    child: Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  buildIcon(),
                  buildIcon(),
                  buildIcon(),
                  buildIcon(),
                  buildIcon(),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "There are many variations of passages of Lorem Ipsum available be the majority have suffered alteration in some form, by injected humour or randomised words.",
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w300),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(
                    backgroundImage: AssetImage("assets/barber1.jpg"),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "There are many.",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
Icon buildIcon() {
  return Icon(
    Icons.star,
    color: Colors.red,
  );
}