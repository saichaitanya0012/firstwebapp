import 'package:animate_do/animate_do.dart';
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mywebdemo/AnimationPage.dart';
import 'package:mywebdemo/HomePage/SlideAnimation.dart';
import 'package:mywebdemo/HomePage/Story.dart';
import 'package:mywebdemo/HomePage/TreatmentData.dart';
import 'package:mywebdemo/HomePage/footer.dart';
import 'package:mywebdemo/Navbar/DeskNavbar.dart';
import 'package:mywebdemo/animation/VeryGoodWidget.dart';

import 'Review.dart';

Widget desktopHomePage(context, s) {
  return Stack(
    children: [
      ListView(
        cacheExtent: 0,
        physics: AlwaysScrollableScrollPhysics(),
        controller: s,
        children: [
          SizedBox(
            height: 95,
          ),

          BounceInDown(manualTrigger: true,controller:(c){
            print(c.value.toString());
            print(c.view.toString());
            c.value;
          },from: 100,child: Text("jdvnksjbvkhjkjvfjbdsvb")),
          Container(
            color: Colors.black,
            child: DelayedDisplay(
              delay: Duration(
                milliseconds: 500,
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: Container(
                          width: MediaQuery.of(context).size.width / 4,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child:
                                SvgPicture.asset("assets/scissors.svg"),
                              ),
                              Text(
                                "WE'RE THE LAST OF BREED",
                                style: TextStyle(
                                    fontSize: 55,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Text(
                                  "Archive your dream style",
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              bookNow()
                            ],
                          )),
                    ),
                  ),
                  Expanded(
                      flex: 3,
                      child: DelayedDisplay(
                          delay: Duration(
                            milliseconds: 750,
                          ),
                          child: Image.asset("assets/h1_hero1.jpg")))
                ],
              ),
            ),
          ),
          BounceInDown(manualTrigger: true,controller:(c){
            print(c.value.toString());
            print(c.view.toString());
            c.value;
          },from: 100,child: Text("jdvnksjbvkhjkjvfjbdsvb")),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                  flex: 2,
                  child:
                  VeryGoodWidget( AnimationPage(
                      child: padding("assets/services1.svg","Stylish hair cut","Available be the majority have suffered alteration in some form, by injected humour.",Colors.black,Colors.white),
                      child1: padding("assets/services1.svg","Stylish hair cut","Available be the majority have suffered alteration in some form, by injected humour.",Colors.white,Colors.black),
                      value: 1,
                    ),
                  )
              ),Expanded(
                  flex: 2,
                  child:
                  VeryGoodWidget( AnimationPage(
                      child: padding("assets/services2.svg","Stylish hair cut","Available be the majority have suffered alteration in some form, by injected humour.",Colors.black,Colors.white),
                      child1: padding("assets/services2.svg","Stylish hair cut","Available be the majority have suffered alteration in some form, by injected humour.",Colors.white,Colors.black),
                      value: 1,
                    ),
                  )
              ),Expanded(
                  flex: 2,
                  child:
                  VeryGoodWidget( AnimationPage(
                      child: padding("assets/services3.svg","Stylish hair cut","Available be the majority have suffered alteration in some form, by injected humour.",Colors.black,Colors.white),
                      child1: padding("assets/services3.svg","Stylish hair cut","Available be the majority have suffered alteration in some form, by injected humour.",Colors.white,Colors.black),
                      value: 1,
                    ),
                  )
              ),
            ],
          ),

          BounceInDown(manualTrigger: true,controller:(c){
            print(c.value.toString());
            print(c.view.toString());
            c.value;
          },from: 100,child: Text("jdvnksjbvkhjkjvfjbdsvb")),
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: Row(
              children: [
                Expanded(flex: 1, child: VeryGoodWidget( buildColumn())),
                Expanded(
                  flex: 1,
                  child: VeryGoodWidget( image1()),
                ),
                Expanded(
                  flex: 1,
                  child: VeryGoodWidget( story()),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: VeryGoodWidget( Column(
                    children: [
                      Row(
                        children: [
                          Expanded(child: Image.asset("assets/barber1.jpg")),
                          Expanded(child: Image.asset("assets/barber2.jpg")),
                          Expanded(child: Image.asset("assets/barber3.jpg")),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(child: Image.asset("assets/barber4.jpg")),
                          Expanded(child: Image.asset("assets/barber5.jpg")),
                          Expanded(child: Image.asset("assets/barber6.jpg")),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              BounceInDown(manualTrigger: true,controller:(c){
                print(c.value.toString());
                print(c.view.toString());
                c.value;
              },from: 100,child: Text("jdvnksjbvkhjkjvfjbdsvb")),
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(50.0),
                    child: VeryGoodWidget( Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: 50,
                            height: 10,
                            color: Colors.red,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Text(
                              "OUR TOP PRICES",
                              style: TextStyle(
                                  fontSize: 55,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Divider(
                              height: 2,
                              color: Colors.black,
                            ),
                          ),
                          beardTreatment("Special Beard Treatment", "From ",
                              "\$40", context),
                          beardTreatment("Special Beard Treatment", "From",
                              " \$40", context),
                          beardTreatment(
                              "Color your Beard", "From ", "\$40", context),
                          beardTreatment(
                              "Wax your Beard", "From ", "\$40", context),
                          beardTreatment(
                              "Wax your Beard", "From", " \$40", context),
                        ],
                      ),
                    ),
                  )),
            ],
          ),

          BounceInDown(manualTrigger: true,controller:(c){

          },from: 100,child: Text("jdvnksjbvkhjkjvfjbdsvb")),
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: VeryGoodWidget( Column(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 50,
                    height: 10,
                    color: Colors.red,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      "CUSTOMERS SAY ABOUT US",
                      style: TextStyle(
                          fontSize: 55,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: [
                        Expanded(flex: 2, child: buildContainer(false)),
                        Expanded(flex: 2, child: buildContainer(true)),
                        Expanded(flex: 2, child: buildContainer(true)),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          BounceInDown(from: 100,child: Text("jdvnksjbvkhjkjvfjbdsvb")),
          Container(
            color: Colors.black,
            child: VeryGoodWidget( Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 30),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Center(child: contactInfo(Icons.phone)),
                        ),
                        Expanded(
                          flex: 1,
                          child: siteInfo(),
                        ),
                        Expanded(
                          flex: 1,
                          child: Center(child: contactInfo(Icons.navigation)),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.white38,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 30),
                    child: Text(
                      "Copyright ©2021 All rights reserved | This template is made with  by Colorlib",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      DeskNavBar(),
    ],
  );
}


