// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// extension hoveranimations on Widget {
//   hoverAnimation({
//     bool value = false,
//   }) {
//     return MouseRegion(
//       onHover: (e) {
//         value=true;
//       },
//       onExit: (e){
//         value=false;
//       },
//       child: AnimatedContainer(
//         child: this,
//         duration: Duration(milliseconds: 250),
//         height: value?50:20,
//       ),
//     );
//   }
// }
//
// extension Neumorphism on Widget {
//   addNeumorphism({
//     double borderRadius = 10.0,
//     Offset offset = const Offset(5, 5),
//     double blurRadius = 10,
//     Color topShadowColor = Colors.white60,
//     Color bottomShadowColor = const Color(0x26234395),
//   }) {
//     return Container(
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
//         boxShadow: [
//           BoxShadow(
//             offset: offset,
//             blurRadius: blurRadius,
//             color: bottomShadowColor,
//           ),
//           BoxShadow(
//             offset: Offset(-offset.dx, -offset.dx),
//             blurRadius: blurRadius,
//             color: topShadowColor,
//           ),
//         ],
//       ),
//       child: this,
//     );
//   }
// }
import 'package:flutter/material.dart';
import 'dart:html' as html;

import 'package:flutter/rendering.dart';
import 'package:sizer/sizer.dart';


extension HoverExtensions on Widget {
  static final appContainer =
  html.window.document.getElementById('app-container');

  Widget get showCursorOnHover {
    return MouseRegion(
      child: this, // the widget we're using the extension on
      onHover: (event) => appContainer.style.cursor = 'pointer',
      onExit: (event) => appContainer.style.cursor = 'default',
    );
  }

  Widget  moveUpOnHover() {
    return TranslateOnHover(
      child: this,
    );
  }
}



// import 'package:flutter/material.dart';

class TranslateOnHover extends StatefulWidget {
  final Widget child;
  TranslateOnHover({Key key, this.child}) : super(key: key);

  @override
  _TranslateOnHoverState createState() => _TranslateOnHoverState();
}

class _TranslateOnHoverState extends State<TranslateOnHover> {
  final nonHoverTransform = Matrix4.identity();
  final hoverTransform = Matrix4.identity()..translate(0, -10, 0);

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: MouseCursor.defer,
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        child: widget.child,
        transform: _hovering ? hoverTransform : nonHoverTransform,
      ),
    );
  }

  void _mouseEnter(bool hovering) {
    setState(() {
      _hovering = hovering;
    });
  }
}