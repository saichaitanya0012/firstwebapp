
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:mywebdemo/HomePage/DesktopHomePage.dart';
import 'package:mywebdemo/HomePage/MobileHomeScreen.dart';
import 'package:mywebdemo/HomePage/tabletHomeScreen.dart';
import 'package:mywebdemo/responsive.dart';
import 'package:provider/provider.dart';

class HomePage extends HookWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }
//
// class _HomePageState extends State<HomePage> {


  @override
  Widget build(BuildContext context) {
    final scrollController = useScrollController();
    return Scaffold(
      backgroundColor: Color(0xFFEBEDFA),
      body: ChangeNotifierProvider.value(
        value: scrollController,
        child: Responsive(
          mobile: MobileHomeScreen(scrollController:scrollController,),
          tablet: tabletHomeScreen(context,scrollController),
          desktop: desktopHomePage(context,scrollController),
        ),
      )
    );
  }
}
